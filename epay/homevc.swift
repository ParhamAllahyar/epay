//
//  homevc.swift
//  epay
//
//  Created by Parham on 9/30/1397 AP.
//  Copyright © 1397 Parham. All rights reserved.
//

import UIKit

class homevc: UIViewController {

    @IBOutlet weak var mapbtn: UIButton!
    
    @IBOutlet weak var homecollection: UICollectionView!
    
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapbtn.layer.cornerRadius = mapbtn.frame.height / 2
        mapbtn.layer.shadowOpacity = 0.4
    }
    

    

}
extension homevc : UICollectionViewDataSource , UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = homecollection.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        cell.contentView.layer.cornerRadius = cell.frame.height / 2
        cell.contentView.layer.borderWidth = 5
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = true
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
        
        cell.contentView.layer.cornerRadius = 80000
        cell.contentView.layer.masksToBounds = true
        
        
        return cell
    }
}
