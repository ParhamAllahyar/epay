//
//  mapViewController.swift
//  epay
//
//  Created by Parham on 9/30/1397 AP.
//  Copyright © 1397 Parham. All rights reserved.
//

import UIKit
import GoogleMaps
class mapViewController: UIViewController {
// AIzaSyD2iMRcEQTi4btH6vEiSNhRzldbnd9YPIg
    override func viewDidLoad() {
        super.viewDidLoad()

        let camera = GMSCameraPosition.camera(withLatitude:35.69439 , longitude:51.42151 , zoom: 18.0)
        let mapView = GMSMapView.map(withFrame: .zero, camera: camera)
        self.view = mapView
        
        let marker = GMSMarker(position: CLLocationCoordinate2D(latitude:28.617220 , longitude: 77.208099))
        marker.title = "tehran"
        marker.snippet = "Tehran , Iran"
        marker.map = mapView
        
    }
    


}
